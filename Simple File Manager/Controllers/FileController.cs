﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Simple_File_Manager.Controllers
{
    public class FileController : Controller
    {
        // GET: File
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult FileManager()
        {
            return View();
        }

        [HttpPost]
        public JsonResult SaveFile()
        {
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];
                var fileName = Path.GetFileName(file.FileName);
                var path = "/Files/" + fileName;
                var fullPath = Server.MapPath(path);
                file.SaveAs(fullPath);
                var fileInfo = new FileInfo(fullPath);

                var fileData = new Models.FileModel
                {
                    CreationTime = fileInfo.CreationTime,
                    FileName = fileInfo.Name,
                    FileSize = fileInfo.Length / 1024,
                    FileExtension = fileInfo.Extension.Split('.')[1]
                };

                return Json(fileData, JsonRequestBehavior.AllowGet);
            }
            return Json("ERROR");
        }

        [HttpPost]
        public JsonResult RenameFile(string NewName, string OldName)
        {
            var oldPath = Server.MapPath("/Files/" + OldName);
            var newPath = Server.MapPath("/Files/" + NewName);
            if (!System.IO.File.Exists(newPath))
            {
                System.IO.File.Move(oldPath, newPath);
                var fileInfo = new FileInfo(newPath);
                return Json(fileInfo.Name, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("ERROR");
            
        }

        
    }
}