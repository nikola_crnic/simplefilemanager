﻿function parseDate(date) {
    var monthNames = [
      "Jan", "Feb", "Mar",
      "Apr", "May", "Jun", "Jul",
      "Aug", "Sep", "Oct",
      "Nov", "Dec"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
    var hours = date.getHours();
    var minutes = date.getMinutes();

    var parsedDate = monthNames[monthIndex] + ' ' + day + ', ' + year + ' - ' + hours + ':' + minutes;

    return parsedDate;
}

