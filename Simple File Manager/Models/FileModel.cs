﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Simple_File_Manager.Models
{
    public class FileModel
    {
        public DateTime CreationTime { get; set; }

        public string FileName { get; set; }

        public decimal FileSize { get; set; }

        public string FileExtension { get; set; }
    }
}