﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Simple_File_Manager.Startup))]
namespace Simple_File_Manager
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
